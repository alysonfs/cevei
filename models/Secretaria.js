/**
 * Models Secretaria.js
 * @date 19-01-2016
 * @author Alyson Felipe<alysonforever@gmail.com>
 */
module.exports = function(app) {
    'use strict';
    var db = app.get('db');
    var Model = app.models.Model;
    class Secretaria {
        
        constructor(id, nome, sigla, endereco, status){
            if(id!=null){this.id = id}
            this.obj = {nome, sigla, endereco, status} ;           
        }
        findAll(){
            return db.secretaria.findSync();
        }
        
    }
    return Secretaria;
}