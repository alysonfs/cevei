/**
 * Models Lotaca.js
 * @date 19-01-2016
 * @author Alyson Felipe <alysonforever@gmail.com>
 */
module.exports = function(app){
    "use strict"
    var db = app.get('db');
    class Lotacao {
        constructor(id, secretaria_id ,nome, sigla, endereco, status){
            if(id!=null){this.id = id}
            this.secretaria_id = secretaria_id;
            this.obj = {nome, sigla,endereco, status};
        }
        save(){
            db.lotacao.save(this);
        }
        findAll(){
            return db.lotacao.findSync();
        }        
    }
    
    return Lotacao;
}