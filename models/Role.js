/**
 * Models Role.js
 * @date 12-01-2016
 * @author Alyson Felipe<alysonforever@gmail.com>
 */
module.exports = function(app){
    "use strict";    
    var db = app.get('db');
    class Role{
        constructor(id, nome, descricao, status){
            if(id!=null){this.id = id}
            this.obj = {nome, descricao, status}            
        }
        // Métodos de trasação
        save(){
            db.role.save({obj:this}, function(error, inserted){
                if (error) throw error;
                //console.log(inserted);
            });
        }
        edit(id){
            db.role.save({id:id, obj:this}, function(error, updated){
                if (error) throw error;
                //console.log(updated);
            })
        }
        del(id){
            db.role.destroy({id:id, obj:this}, function(error, destroyed){
                if (error) throw error;
                //console.log(destroyed);
            });
        }
        findAll(){            
            return db.role.findSync();
        }
        findById(id){
            return db.role.find({id:id}, function(error, role){
                if (error) throw console.log(error);
            });
        }
    }
    // Retorna a class Role
    return Role;
}