/**
 * Models Motivo.js
 * @date 21-01-2016
 * @author Alyson Felipe <alysonforever@gmail.com>
 */
module.exports = function(app){
    "use strict";
    var db = app.get('db');
    class Motivo {
        constructor(id, nome, descricao, status){
            if(id!=null){this.id = id}
            this.obj = {nome, descricao, status};
        }
        findAll(){
            return db.motivo.findSync();
        }
        save(){
            db.motivo.save(this, function(error, inserted){
                if(error){console.log(error)}
            });
        }
    }
    return Motivo;
}