/**
 * Models Solicitar.js
 * @date 19-01-2016
 * @author Alyson Felipe<alysonforever@gmail.com>
 */
module.exports = function (app) {
    "use strict";
    var db = app.get('db');
    var local = new app.models.Local;
    class Solicitar {
        constructor(id, usuario_id, origem_id, destino_id, motivo_id, qtd, obs, atendimento, status) {
            if (id != null) { this.id = id }
            this.user_id = usuario_id;
            this.origem_id = origem_id;
            this.destino_id = destino_id;
            this.motivo_id = motivo_id;
            this.obj = { qtd, obs, atendimento, status };
        }     
        find(query, option, callback){            
                db.solicitar.find(query, option, callback);            
        }         
        findAll() {
            return db.runSync(
                "select s.id, s.obj, s.user_id ,u.obj as usuario, o.id as origem_id, o.obj as origem, d.id as destino_id, d.obj as destino, m.id as motivo_id, m.obj as motivo  from solicitar s inner join local o on s.origem_id = o.id inner join local d on s.destino_id = d.id inner join motivo m on s.motivo_id = m.id inner join public.user u on s.user_id = u.id"
                );
        }
        findAllByUser(id) {
            return db.runSync(
                "select s.id, s.obj, s.user_id ,u.obj as usuario, o.id as origem_id, o.obj as origem, d.id as destino_id, d.obj as destino, m.id as motivo_id, m.obj as motivo  from solicitar s inner join local o on s.origem_id = o.id inner join local d on s.destino_id = d.id inner join motivo m on s.motivo_id = m.id inner join public.user u on s.user_id = u.id where s.user_id = $1", [id]
                );
        }
        findAllBySec(id) {
            return db.runSync(
                "select s.id, s.obj, s.user_id ,u.obj as usuario, o.id as origem_id, o.obj as origem, d.id as destino_id, d.obj as destino, m.obj->>'nome' as motivo  from solicitar s inner join local o on s.origem_id = o.id inner join local d on s.destino_id = d.id inner join motivo m on s.motivo_id = m.id inner join public.user u on s.user_id = u.id  inner join lotacao l on u.lotacao_id = l.id where l.id = $1", [id]
                );
        }
        findById(id) {
            return db.runSync(
                "select s.id, s.obj, s.user_id ,u.obj as usuario, o.id as origem_id, o.obj as origem, d.id as destino_id, d.obj as destino, m.id as motivo_id, m.obj as motivo from solicitar s inner join local o on s.origem_id = o.id inner join local d on s.destino_id = d.id inner join motivo m on s.motivo_id = m.id inner join public.user u on s.user_id = u.id inner join lotacao l on u.lotacao_id = l.id where s.id = $1", [id]
                )[0];
        }
        save(callback) {            
            db.solicitar.save(beforeSave(this), callback);
        }
        update(callback) {
            db.solicitar.save(beforeSave(this), callback);
        }
        del(id, callback){
            db.solicitar.destroy({id:id}, callback);
        }        
    }
    
    function beforeSave(solicitar) {
        var origem;
        var destino;
        try {
            origem = local.findById(parseInt(solicitar.origem_id));
        } catch (error) {
            origem = local.findByEndereco(solicitar.origem_id);
        }
        try {
            destino = local.findById(parseInt(solicitar.destino_id));
        } catch (error) {
            destino = local.findByEndereco(solicitar.destino_id);    
        } 
        
        if (origem !== undefined) {
            solicitar.origem_id = origem.id
        } else {
            local = new app.models.Local(
                null,
                solicitar.origem_id,
                '',
                solicitar.origem_id,
                '',
                '',
                'novo'
                );
            solicitar.origem_id = db.local.saveSync(local).id;
        }
        if (destino !== undefined) {
            solicitar.destino_id = destino.id;
        } else {
            local = new app.models.Local(
                null,
                solicitar.destino_id,
                '',
                solicitar.destino_id,
                '',
                '',
                'novo'
                );
            solicitar.destino_id = db.local.saveSync(local).id;
        }
        return solicitar;
    }
    // Retorna a class Solicitar
    return Solicitar;
}