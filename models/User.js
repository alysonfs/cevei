/**
 * Models User.js
 * @date 12-01-2016
 * @author Alyson Felipe<alysonforever@gmail.com>
 */
module.exports = function (app) {
    "use strict";
    var db = app.get('db');
    class User {        
        constructor(id, role_id, lotacao_id, nome, matricula, email, telefone, celular) {
            if(id!=null){this.id = id}            
            this.role_id = role_id;
            this.lotacao_id = lotacao_id;            
            this.obj = {nome, matricula, email, telefone, celular };            
        }
        save() {            
            db.user.save(this, function (error, inserted) {
                if(error){console.log(error)}
            });
        }
        edit(id) {
            db.user.save({ id: id, obj: this }, function (error, updated) {
                console.log(error);
                console.log(updated);
            })
        }
        del(id) {
            db.user.destroy({ id: id, obj: this }, function (error, destroyed) {
                console.log(error);
                console.log(destroyed);
            });
        }
        findAll() {            
            return db.user.findSync();
        }
        getUser(id){
            return db.user.findSync(id);
        }
        getUserByMatricula(matricula){                         
            return db.runSync("select u.*, l.obj as lotacao, l.secretaria_id, s.obj as secretaria from public.user u inner join lotacao l on u.lotacao_id = l.id inner join secretaria s on l.secretaria_id = s.id where u.obj->>'matricula' = $1", [matricula]);
        }
        logar(matricula){
            var usuario = this.getUserByMatricula(matricula);            
            if(usuario){
                return true;
            }else{
                return false;
            }
        }
        
    }
    // Retorna a class User
    return User;
}