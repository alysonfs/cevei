/**
 * Models Local.js
 * @date 21-01-2016
 * @author Alyson Felipe <alysonforever@gmail.com>
 */

module.exports = function(app){
    "use strict"
    var db = app.get('db');
    class Local{
        constructor(id, nome, sigla, endereco, lat, lng, status){
            if(id!=null){this.id = id}
            var map = lat+","+lng;
            this.obj = {nome, sigla, endereco,geo:{lat,lng, map}, status}
        }
        findAll(){
            return db.local.findSync({},{order:"obj->>'nome'"});
        }
        save(callback){            
            db.local.save(this, callback);                        
        }        
        findById(id){
            return db.local.findSync({id})[0];
        }
        findByEndereco(endereco){
            return db.runSync("select * from public.local l where lower(l.obj->>'endereco') = lower($1);",[endereco])[0];
        }
        update(callback){
            db.local.save(this, callback);
        }
    }    
    return Local;
}