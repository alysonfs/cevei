/**
 * Controller Solicitar.js
 * @date 18-01-2016
 * @author Alyson Felipe <alysonforever@gmail.com>
 */
module.exports = function (app) {
    "use strict"
    var solicitar = new app.models.Solicitar();
    var motivo = new app.models.Motivo;
    var local = new app.models.Local;

    var SolicitarController = {
        // Property of controller
        index: function (req, res, next) {
            var params = {
                page: 'solicitacoes',
                solicitacoes: [],
                usuario: req.session.usuario
            };
            if (req.session.usuario.role_id == 1) {
                params['solicitacoes'] = solicitar.findAll();
            } else if (req.session.usuario.role_id == 2) {
                params['solicitacoes'] = solicitar.findAllBySec(req.session.usuario.secretaria_id);
            } else {
                params['solicitacoes'] = solicitar.findAllByUser(req.session.usuario.id);
            }
            res.render('solicitar/index', params);
        },
        add: function (req, res, next) {
            var params = {
                page: 'adicionar',
                locais: local.findAll(),
                motivos: motivo.findAll(),
                usuario: req.session.usuario
            };
            res.render('solicitar/add', params);
        },
        // Actions of controller
        doCreate: function (req, res, next) {
            var solicitar = new app.models.Solicitar(
                null,
                req.session.usuario.id,
                req.body.solicitar.origem_id,
                req.body.solicitar.destino_id,
                req.body.solicitar.motivo_id,
                req.body.solicitar.qtd || 1,
                req.body.solicitar.obs,
                {},
                "novo"
                );
            solicitar.save(function (error, inserted) {
                if (error) { console.log(error) }
                res.redirect('/solicitar/index');
            });

        },
        edit: function (req, res, next) {
            var params = {
                page: 'editar',
                locais: local.findAll(),
                motivos: motivo.findAll(),
                usuario: req.session.usuario,
                solicitar: solicitar.findById(req.params.id)
            }
            res.render('solicitar/edit', params);
        },
        doEdit: function (req, res, next) {
            var solicitar = new app.models.Solicitar(
                req.params.id,
                req.session.usuario.id,
                req.body.solicitar.origem_id,
                req.body.solicitar.destino_id,
                req.body.solicitar.motivo_id,
                req.body.solicitar.qtd || 1,
                req.body.solicitar.obs,
                {},
                "novo"
                );
            solicitar.update(function () {
                res.redirect('/solicitar/index');
            });
        },
        doDel: function (req, res, next) {
            solicitar.del(req.params.id, function () {
                res.redirect('/solicitar/index');
            });

        },
        doAutorizar: function (req, res, next) {
            var solicitar = new app.models.Solicitar(
                req.solicitar.id,
                req.solicitar.user_id,
                req.solicitar.origem_id,
                req.solicitar.destino_id,
                req.solicitar.motivo_id,
                req.solicitar.obj.qtd,
                req.solicitar.obj.obs,
                {},
                'autorizado'
                );
            solicitar.update(function () {
                res.send(true);
            });
        },
        doDesAutorizar: function (req, res, next) {
            var solicitar = new app.models.Solicitar(
                req.solicitar.id,
                req.solicitar.user_id,
                req.solicitar.origem_id,
                req.solicitar.destino_id,
                req.solicitar.motivo_id,
                req.solicitar.obj.qtd,
                req.solicitar.obj.obs,
                {},
                'desautorizado'
                );
            solicitar.update(function () {
                res.send(true);
            });
        },
        atender: function (req, res, next) {
            var atender = [];
            if (typeof (req.body.atender) == "string") {
                atender[0] = req.body.atender;
            } else {
                atender = req.body.atender;
            }
            var params = {
                page: 'atender',
                usuario: req.session.usuario,
                solicitacoes: atender
            }
            res.render('solicitar/atender', params);
        },
        doAtender: function (req, res, next) {
            var atender = {
                tipo: req.body.atender.tipo,
                placa: req.body.atender.placa,
                motorista: req.body.atender.motorista,
                telefone: req.body.atender.telefone,
                obs: req.body.atender.obs
            }
            var solicitacoes = JSON.parse(req.body.atender.solicitacoes);
            solicitacoes.forEach(function (solicitar, i) {
                solicitar = JSON.parse(solicitar);
                // console.log(solicitar);
                var dbSolicitar = new app.models.Solicitar(
                    solicitar.id,
                    solicitar.user_id,
                    solicitar.origem_id,
                    solicitar.destino_id,
                    solicitar.motivo_id,
                    solicitar.obj.qtd,
                    solicitar.obj.obs,
                    atender,
                    "atendido"
                    );
                    // console.log(dbSolicitar);
                dbSolicitar.update(function (error, uptaded) {
                    if (error) { console.log(error) }                    
                });
            });
            res.redirect('/solicitar/index');
        },
        resolverParam: function (req, res, next, id) {
            solicitar.find(parseInt(id), null, function (error, result) {
                if (error) {
                    next(error);
                } else if (result) {
                    req.solicitar = result;
                    res.locals.solicitar = result;
                    next();
                } else {
                    next(new Error('FALHA para carregra o solicitar'));
                }
            });
        }
    }

    return SolicitarController;
}