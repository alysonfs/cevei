/**
 * Controller Secretaria.js
 * @date 19-01-2016
 * @author Alyson Felipe<alysonforever@gmail.com>
 */
module.exports = function(app){
    "use strict"
    var db = app.get('db');
    var secretaria = new app.models.Secretaria;
    var SecretariaController = {
        // Property of object
        index: function(req, res, next){
            var params = {
                page:  'secretarias',
                usuario: req.session.usuario,
                secretarias: secretaria.findAll()
            }
            res.render('secretaria/index', params);            
        },
        add: function(req, res, next){
            var params = {
                page:  'adicionar',
                usuario: req.session.usuario                
            } 
            res.render('secretaria/add', params);
        },
        edit: function(req, res, next){},
        show: function(req, res, next){},
        // Action of object        
        doCreate: function(req, res, next){
            var secretaria = new app.models.Secretaria(
                req.body.nome,
                req.body.sigla,
                req.body.endereco,
                'ativo'
            )
            db.secretaria.save({obj:secretaria}, function(error, inserted){
                if(error){
                    console.log(error);
                }else{
                    res.redirect('/secretaria/index');
                }
            });
        },
        doDel: function(req, res, next){},
        doUpdate: function(req, res, next){}        
    }
    
    return SecretariaController;
}