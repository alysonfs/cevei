/**
 * Controller Lotacao.js
 * @date 19-01-2016
 * @author Alyson Felipe<alysonforever@gmail.com>
 */
module.exports = function(app){
    "use strict"
    var db = app.get('db');
    var LotacaoController = {
        // Property of object
        index: function(req, res, next){
            var params = {
                page:  'lotacoes',
                usuario: req.session.usuario,
                lotacoes: [],
                secretarias: []
            }
            db.lotacao.find(function(error, lotacoes){
                params['lotacoes'] = lotacoes;                       
                res.render('lotacao/index', params);
            });
        },
        add: function(req, res, next){
            var params = {
                page:  'adicionar',
                usuario: req.session.usuario                
            } 
            res.render('lotacao/add', params);
        },
        edit: function(req, res, next){},
        show: function(req, res, next){},
        // Action of object        
        doCreate: function(req, res, next){
            var lotacao = new app.models.Lotacao(
                req.body.nome,
                req.body.sigla,
                req.body.endereco,
                'ativo'
            )
            db.lotacao.save({obj:lotacao}, function(error, inserted){
                if(error){
                    console.log(error);
                }else{
                    res.redirect('/lotacao/index');
                }
            });
        },
        doDel: function(req, res, next){},
        doUpdate: function(req, res, next){}
    }
    
    return LotacaoController;
}