/**
 * Controller Motivo.js
 * @date 22-01-2016
 * @author Alyson Felipe <alysonforever@gmail.com> 
 */
module.exports = function(app){
    "use strict"    
    var motivo = new app.models.Motivo;
    
    var MotivoController = {
        index: function(req, res, next){
            var params = {
                page:"motivos",
                usuario: req.session.usuario,              
                motivos: motivo.findAll()
            }
            res.render('motivo/index', params);
        },
        add: function(req, res, next){
            var params = {
                page:"cadastrar",
                usuario: req.session.usuario,                              
            }
            res.render('motivo/add', params);
        },
        doCreate: function(req, res, next){
            var motivo = new app.models.Motivo(
                 null,
                 req.body.motivo.nome,
                 req.body.motivo.descricao,
                 "ativo"
            )
            motivo.save();
            res.redirect('/motivo/index');               
        }
    }
    
    return MotivoController;
}