/**
 * Controller User.js
 * @date 14-01-2016
 * @author Alyson Felipe <alysonforever@gmail.com>
 */
module.exports = function (app) {
    "use strict"
    var user = new app.models.User;
    var secretaria = new app.models.Secretaria;
    var lotacao = new app.models.Lotacao;    
    var role = new app.models.Role;
    var solicita = {};
        
    var UserController = {
        // Propetys of render
        index: function (req, res, next) {
            var params = {
                page: 'usuarios',                
                usuario:req.session.usuario,
                usuarios: user.findAll()
            };                                                
            res.render('user/index', params);
        },
        home: function (req, res, next) {
            var usuario = req.session.usuario;            
            var params = {
                page: 'home',                
                usuario: usuario
            };                        
            res.render('user/home', params);                            
        },
        login: function (req, res, next) {
            var session = req.session;
            // Definir os parametros que serão renderizados na pagina
            var params = {
                page: 'login',
                usuario:null
            };
            // Testar se existe um usuario salvo na sessao
            if (session.usuario) {
                // add o usuario, logado, a sessao
                params['usuario'] = session.usuario;
            } else {
                // inserindo usuario em branco para formuario de login
                //var usuario = { matricula: '', nome: '', secretaria: '', lotacao: '', role_id: '' };
               // params['usuario'] = usuario;
            }
            res.render('user/login', params);
        },
        
        doAutoCreate: function (req, res, next){
            if(req.body.user){
                var usuario = new app.models.User(
                    null,
                    3,
                    req.body.user.lotacao_id,
                    req.body.user.nome,
                    req.body.user.matricula,
                    req.body.user.email,
                    req.body.user.telefone,
                    req.body.user.celular
                );
                usuario.save();
            }                      
            res.redirect('/');
        },
        doCreate: function (req, res, next){
            if(req.body.user){
                var usuario = new app.models.User(
                    null,
                    req.body.user.role_id,
                    req.body.user.lotacao_id,
                    req.body.user.nome,
                    req.body.user.matricula,
                    req.body.user.email,
                    req.body.user.telefone,
                    req.body.user.celular
                );
                usuario.save();
            }                        
            res.redirect('/user/index');          
        },
        
        doLogin: function (req, res, next) {
            var matricula = req.body.user.matricula;                                  
            if (user.logar(matricula)) {                                   
                    req.session.usuario = user.getUserByMatricula(matricula)[0];                    
                    res.redirect('user/home')
            } else {
                req.session.usuario = null;
                res.redirect('user/login');
            }
        },
        doLogout: function (req, res, next) {            
            req.session.destroy();
            res.redirect('/');
        },
        add: function (req, res, next) {
            var session = req.session;
            // Definir os parametros que serão renderizados na pagina
            var params = {
                page: 'cadastrar',
                secretarias: secretaria.findAll(),
                lotacoes: lotacao.findAll(),
                roles: role.findAll(),
                usuario:req.session.usuario
            };            
            res.render('user/add', params);
        },
        cadastro: function (req, res, next) {            
            // Definir os parametros que serão renderizados na pagina
            var params = {
                page: 'cadastro',
                secretarias: secretaria.findAll(),
                lotacoes: lotacao.findAll(),
                usuario:req.session.usuario             
            };            
            res.render('user/cadastro', params);
        },
        edit: function (req, res, next) {
            res.render('user/edit', { page: 'editar' });
        }
    }
    return UserController;
}