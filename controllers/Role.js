/**
 * Controller Role.js
 * @data 12-01-2016
 * @author Alyson Felipe <alysonforever@gmail.com> 
 */
module.exports = function(app){
    "use strict"        
    var RoleController = {
        // Propetys of render
        index:function(req, res, next){                           
            res.render('role/index', {page:'perfis'});
        },
        view: function(req, res, next){
            res.render('role/view', {page: 'visualizar'});
        },
        add: function(req, res, next){
            res.render('role/add', {page: 'adicionar'});
        },
        edit: function(req, res, next){
            res.render('role/edit', {page: 'editar'});
        },
        // Propety of action
        doCreate:function(req, res, next){
            var role = new app.models.Role(
                req.body.role.nome,
                req.body.role.descricao
            );
            role.save(function(){
                res.redirect('/');
            });            
        }
        
    }
    return RoleController;
}