/**
 * Cotroller Local.js
 * @date 25-01-2016
 * @author Alyson Felipe <alysonforever@gmail.com>
 */
module.exports = function(app){
    "use strict"
    var local = new app.models.Local;
    var LocalController = {
        index: function(req, res, next){
            var params = {
                page : 'locais',
                usuario : req.session.usuario,
                locais : local.findAll()
            }
            res.render('local/index', params);
        },
       add: function(req, res, next){
            var params = {
                page : 'cadastrar',
                usuario : req.session.usuario                
            }
           res.render('local/add', params);
       }, 
       doCreate: function(req, res, next){
           res.redirect('/local/index');
       },
       edit: function(req, res, next){                      
           var params = {
               page: 'editar',
               usuario: req.session.usuario,
               local: local.findById(req.params.id)
           }           
           res.render('local/edit', params);
       },
       doEdit: function(req, res, next){
           var local = new app.models.Local(
               req.params.id,
               req.body.local.nome.toUpperCase(),
               req.body.local.sigla.toUpperCase(),
               req.body.local.endereco.toUpperCase(),
               req.body.local.lat,
               req.body.local.lng,
               req.body.local.status.toUpperCase()
           );
           console.log(local.update());
           res.redirect('/solicitar/index');
           
       }
    }
    return LocalController;
}