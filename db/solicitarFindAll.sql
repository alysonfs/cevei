select s.id, s.obj, o.obj->>'nome'as origem, d.obj->>'nome' as destino, m.obj->>'nome' as motivo 
from solicitar s  
inner join local o on s.origem_id = o.id 
inner join local d on s.destino_id = d.id 
inner join motivo m on s.motivo_id = m.id;