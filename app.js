var http = require('http');
var express = require('express');
var load = require('express-load');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var expressSession = require('express-session');
var massive = require('massive');
var connectionString = 'postgres://postgres:p0stgr3s%234dm1n@192.168.1.10:5432/cevei';
var moment = require('moment');

var app = express();
var massiveInstance = massive.connectSync({ connectionString: connectionString });
var google_maps_api_key = 'AIzaSyDDmrTob5-vTqfhaZ-A3YIFyjiRTANPoGM';

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(cookieParser('b25b5596722af36e960daeb9631dfbe2')); // md5(cevei) - http://www.sha1-online.com/
app.enable('trust proxy');
// app.set('trust proxy', 1) // trust first proxy 

// Implementando o controle de sessao; 
app.use(expressSession({
    secret: 'b25b5596722af36e960daeb9631dfbe2', // md5(cevei) - http://www.sha1-online.com/
    resave: false,
    saveUninitialized: true,
    usuario: null,
    cookie: { secure: false, maxAge: 1800000 }
    // cookie: { secure: false, maxAge: 60000 }
}));
app.set('db', massiveInstance);
app.set('moment', moment);
app.set('maps', google_maps_api_key);
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(methodOverride('_method'));
app.use(express.static(path.join(__dirname, 'public')));

//load('models/Model.js').
load('models/Model.js').
    then('models').    
    then('controllers').
    then('routes').
    into(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.listen(3000, function () {
    console.log('ligou');
    
});

module.exports = app;