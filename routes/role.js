/**
 * Router role.js
 * @date 12-01-2016
 * @author Alyson Felipe <alysonforever@gmail.com>
 */
module.exports = function(app) {
    var roleController = app.controllers.Role;
    var router = require('express').Router();
    
    router.get('/index', roleController.index);    
    router.get('/add', roleController.add);
    router.post('/add', roleController.doCreate);
    // Aplica o role à aplicação
    app.use('/role', router);
}