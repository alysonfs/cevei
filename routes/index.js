module.exports = function (app) {
    var router = require('express').Router();
    var userController = app.controllers.User;

    router.get('/', function (req, res, next) {
        var session = req.session;
        var params = {
            page: 'index',
            usuario: null
        };
        if (session) {
            if (session.usuario) {
                params['usuario'] = session.usuario;                
            }
            res.render('index', params);            
        }else{
            // arrumar uma forma de criar a sessao.
        }

    });
    
    // Rotas pertecentes ao usuario iniciados na raiz
    router.post('/login', userController.doLogin);

    app.use(router);
}