/**
 * Routes local.js
 * @date 25-01-2016
 * @author Alyson Felipe <alysonforever@gmail.com>
 */
module.exports = function(app){
    "use strict"
    var auth = require('../middlewares/Autenticador');
    var localController = app.controllers.Local;
    var router = require('express').Router();    
    
    router.get('/index', auth, localController.index);
    router.get('/add', auth, localController.add);
    router.post('/add', auth, localController.doCreate);
    router.get('/:id/edit', auth, localController.edit);
    router.post('/:id/edit', auth, localController.doEdit);
    // Aplica o local a apliação;
    app.use('/local', router);
}