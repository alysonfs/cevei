/**
 * Routes secretaria.js
 * @date 19-01-2016
 * @author Alyson Felipe<alysonforever@gmail.com>
 */
module.exports = function(app){
    "use strict"
    var auth = require('../middlewares/Autenticador');
    var secretariaController = app.controllers.Secretaria;
    var router = require('express').Router();    
    
    router.get('/index', auth, secretariaController.index);
    router.get('/add', auth, secretariaController.add);
    router.post('/add', auth, secretariaController.doCreate);
    router.get('/:id/del', auth, secretariaController.doDel);
    // Aplica a secretaria na apliacao
    app.use('/secretaria', router);
}