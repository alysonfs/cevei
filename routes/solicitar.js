/**
 * Router solicitar.js
 * @date 18-01-2016
 * @author Alyson Felipe <alysonforever@gmail.com>
 */
module.exports = function(app){
    "use strict"
    var auth = require('../middlewares/Autenticador');
    var solicitarController = app.controllers.Solicitar;
    var router = require('express').Router(); 
    
    router.param('id', solicitarController.resolverParam);
    
    router.get('/index', auth, solicitarController.index);
    router.get('/add', auth, solicitarController.add);
    router.post('/add', auth, solicitarController.doCreate);
    router.get('/:id/edit', auth, solicitarController.edit);
    router.post('/:id/edit', auth, solicitarController.doEdit);
    router.get('/:id/del', auth, solicitarController.doDel);
    router.post('/:id/autorizar', auth, solicitarController.doAutorizar);
    router.post('/:id/desautorizar', auth, solicitarController.doDesAutorizar);
    router.post('/atendimento', auth, solicitarController.atender);
    router.post('/atender', auth, solicitarController.doAtender);
    // Aplica o solicitar a apliação;
    app.use('/solicitar', router);
}
