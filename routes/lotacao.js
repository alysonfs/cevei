/**
 * Routes lotacao.js
 * @date 19-01-2016
 * @author Alyson Felipe<alysonforever@gmail.com>
 */
module.exports = function(app){
    "use strict"
    var auth = require('../middlewares/Autenticador');
    var lotacaoController = app.controllers.Lotacao;
    var router = require('express').Router();    
    
    router.get('/index', auth, lotacaoController.index);
    router.get('/add', auth, lotacaoController.add);
    router.post('/add', auth, lotacaoController.doCreate);
    router.get('/:id/del', auth, lotacaoController.doDel);
    // Aplica a secretaria na apliacao
    app.use('/lotacao', router);
}