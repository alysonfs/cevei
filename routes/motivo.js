/**
 * Router motivo.js
 * @date 22-01-2016
 * @author Alyson Felipe <alysonforever@gmail.com>
 */
module.exports =  function(app){
    "use strict"
    var auth = require('../middlewares/Autenticador');
    var motivoController = app.controllers.Motivo;
    var router = require('express').Router();
    
    router.get('/index', auth, motivoController.index);
    router.get('/add', auth, motivoController.add);
    router.post('/add', auth, motivoController.doCreate);
    
    app.use('/motivo', router); 
}