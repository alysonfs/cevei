/**
 * Router user.js
 * @date 14-01-2016
 * @author Alyson Felipe <alysonforever@gmail.com>
 */
module.exports = function(app){
    var auth = require('../middlewares/Autenticador');
    var userController = app.controllers.User;
    var router = require('express').Router();    
    
    router.get('/index', auth, userController.index);
    router.get('/home', auth, userController.home);
    router.get('/login', userController.login);
    router.get('/logout', auth, userController.doLogout);
    router.get('/add', auth, userController.add);
    router.post('/add', auth, userController.doCreate);
    router.get('/cadastro', userController.cadastro);
    router.post('/cadastro', userController.doAutoCreate);
                
    
    // Aplica o user a apliação;
    app.use('/user', router);
}